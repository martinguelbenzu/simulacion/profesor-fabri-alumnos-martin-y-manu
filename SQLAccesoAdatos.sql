﻿-- Creación de la base de datos
CREATE DATABASE acceso_a_datos;

-- Acceso a la base de datos
USE acceso_a_datos;

-- Limpieza de tablas antes de crear las nuevas 
DROP TABLE IF EXISTS limpiadores;
DROP TABLE IF EXISTS propiedades;
DROP TABLE IF EXISTS limpian;
DROP TABLE IF EXISTS clientes;
DROP TABLE IF EXISTS alojamientos;
DROP TABLE IF EXISTS propietarios_propiedades;

-- Creación de la tabla limpiadores:
-- Atributos: dni , tlf , horas_mensuales
-- Clave principal: dni
CREATE TABLE limpiadores (
  dni varchar(9) NOT NULL,
  tlf varchar(9) NOT NULL,
  horas_mensuales float(5,2) DEFAULT NULL,
  PRIMARY KEY (dni)
  );

-- Creación de la tabla propiedades:
-- Atributos: dir_completa , habitaciones , ascensor
-- Clave principal: dir_completa
CREATE TABLE propiedades (
  dir_completa varchar(50) NOT NULL,
  habitaciones int NOT NULL,
  ascensor boolean DEFAULT NULL,
  PRIMARY KEY (dir_completa)
  );

-- Creación de la tabla limpian:
-- Atributos: id_limpieza , dni , dir_completa , duración_servicio
-- Clave principal: id_limpieza
-- Claves ajenas: dni (limpiadores)
--                dir_completa (propiedades)
-- Clave unica: ( dni + dir_completa )
CREATE TABLE limpian (
  id_limpieza int AUTO_INCREMENT NOT NULL,
  dni varchar(9) NOT NULL,
  dir_completa varchar(50) NOT NULL,
  duracion_servicio float(5,2) NOT NULL,
  PRIMARY KEY (id_limpieza)
  );

-- Creación de la tabla utensilios:
-- Atributos: id_utensilios , id_utensilios , utensilio
-- Clave principal: id_utensilios
-- Clave ajena: id_limpieza (limpian)
-- Clave unica: id_limpieza
CREATE TABLE utensilios (
  id_utensilios int AUTO_INCREMENT,
  id_limpieza int NOT NULL,
  utensilio varchar(30) NOT NULL,
  PRIMARY KEY (id_utensilios)
  );

-- Creación de la tabla clientes:
-- Atributos: dni , tlf , f_nac , precio_firmado , dir_completa
-- Clave principal: dni
-- Clave ajena: dir_completa (propiedades)
-- Clave unica: dir_completa
CREATE TABLE clientes (
  dni varchar(9) NOT NULL,
  tlf varchar(9) NOT NULL,
  f_nac date NOT NULL,
  precio_firmado float(6,2) NOT NULL,
  dir_completa varchar(50) NOT NULL,
  PRIMARY KEY (dni)
  );

-- Creación de la tabla inquilinos:
-- Atributos: dni , tlf , f_nac
-- Clave principal: dni
CREATE TABLE inquilinos (
  dni varchar(9) NOT NULL,
  tlf varchar(9) NOT NULL,
  f_nac date NOT NULL,
  PRIMARY KEY (dni)
  );

-- Creación de la tabla alojamientos:
-- Atributos: id_alojamiento , dni_cliente , dni_inquilino
-- Clave principal: id_alojamiento
-- Clave ajena: dni_cliente (clientes)
--                dni_inquilino (inquilinos)
CREATE TABLE alojamientos (
  id_limpieza int AUTO_INCREMENT NOT NULL,
  dni_cliente varchar(9) NOT NULL,
  dni_inquilino varchar(9) NOT NULL,
  PRIMARY KEY (id_limpieza)
  );

-- Creación de la tabla propietarios:
-- Atributos: dni , tlf , e_mail
-- Clave principal: dni
CREATE TABLE propietarios (
  dni varchar(9) NOT NULL,
  tlf varchar(9) NOT NULL,
  e_mail varchar(50) NOT NULL,
  PRIMARY KEY (dni)
  );

-- Creación de la tabla propietarios_propiedades:
-- Atributos: id_posesion , dni , dir_completa
-- Clave principal: id_posesion
-- Claves ajenas: dni (propietarios)
--                dir_completa (propietarios)
CREATE TABLE propietarios_propiedades (
  id_posesion int AUTO_INCREMENT NOT NULL,
  dni varchar(9) NOT NULL,
  dir_completa varchar(50) NOT NULL,
  PRIMARY KEY (id_posesion)
  );

-- Definición de las relaciones entre tablas y 
-- de las claves unicas de cada tabla 
-- en caso de haberlas.

ALTER TABLE limpian
ADD UNIQUE INDEX uk_dni_dir_completa (dni,dir_completa);

ALTER TABLE limpian
ADD CONSTRAINT fk_limpian_limpiadores FOREIGN KEY (dni)
REFERENCES limpiadores(dni);

ALTER TABLE limpian
ADD CONSTRAINT fk_limpian_propiedades FOREIGN KEY (dir_completa)
REFERENCES propiedades(dir_completa);

ALTER TABLE utensilios
ADD UNIQUE INDEX uk_utensilios (id_limpieza);

ALTER TABLE utensilios
ADD CONSTRAINT fk_utensilios FOREIGN KEY (id_limpieza)
REFERENCES limpian(id_limpieza);

ALTER TABLE clientes
ADD UNIQUE INDEX uk_dir (dir_completa);

ALTER TABLE clientes
ADD CONSTRAINT fk_clientes FOREIGN KEY (dir_completa)
REFERENCES propiedades(dir_completa);

ALTER TABLE alojamientos
ADD UNIQUE INDEX uk_clientes (dni_cliente);

ALTER TABLE alojamientos
ADD UNIQUE INDEX uk_inquilinos (dni_inquilino);

ALTER TABLE alojamientos
ADD CONSTRAINT fk_dni_cliente FOREIGN KEY (dni_cliente)
REFERENCES clientes(dni);

ALTER TABLE alojamientos
ADD CONSTRAINT fk_dni_inquilino FOREIGN KEY (dni_inquilino)
REFERENCES inquilinos(dni);

ALTER TABLE propietarios_propiedades
ADD UNIQUE INDEX uk_dni_dir_completa (dni,dir_completa);

ALTER TABLE propietarios_propiedades
ADD CONSTRAINT fk_dni_propietario FOREIGN KEY (dni)
REFERENCES propietarios(dni);

ALTER TABLE propietarios_propiedades
ADD CONSTRAINT fk_dir_propiedad FOREIGN KEY (dir_completa)
REFERENCES propiedades(dir_completa);